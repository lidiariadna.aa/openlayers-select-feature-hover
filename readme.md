# OpenLayers/ Select Features by Hover

A diferencia del ejercio anterior, este ejemplo, que también registra un listener en el movimiento del puntero del mapa para resaltar la característica sobre la que se encuentra actualmente, ocupa por separado la clase `ol/Style/Style.js`.

Inicalmente crea una capa vectorial cargando un archivo _*GeoJSON*_. Este archivo representa las características geográficas que queremos mostrar en el mapa.

```js 
const vector = new VectorLayer({
  source: new VectorSource({
    url: 'https://openlayers.org/data/vector/ecoregions.json',
    format: new GeoJSON(),
  }),
  background: 'white',
  style: function (feature) {
    const color = feature.get('COLOR') || '#eeeeee';
    style.getFill().setColor(color);
    return style;
  },
});
```

Una vez creada la capa vectorial, el siguiente paso es inicializar el mapa y cargar la capa vectorial creada previamente. Este proceso define el contenedor del mapa, el punto de vista inicial(centro y nivel de zoom), y las capas que incluira el mapa

```js
const map = new Map({
  layers: [vector],
  target: 'map',
  view: new View({
    center: [0, 0],
    zoom: 2,
  }),
});
```

Posteriormente, se crean estilos personalizados utilizando la clase `ol/Style/Style.js` y los metodos `Fill` y `Strocke`. Esta clase actua como un contenedor para diferentes estilos que se pueden aplicar a las características vectoriales. Cabe destacar que cualquier cambio realizado en el estilo de una característica o capa requerirá que se vuelva a renderizar para que los cambios tengan efecto.


```js
const selectStyle = new Style({
  fill: new Fill({
    color: '#eeeeee',
  }),
  stroke: new Stroke({
    color: 'rgba(255, 255, 255, 0.7)',
    width: 2,
  }),
});
```

Finalmente se agrega un evento `pointermove` al mapa. este evento se activa cuando el usuario mueve el cursor sobre el mapa, permitiendo identificar la característica bajo el cursor, en este caso el nombre de las ecoregiones.

```js
let selected = null;
map.on('pointermove', function (e) {
  if (selected !== null) {
    selected.setStyle(undefined);
    selected = null;
  }

  map.forEachFeatureAtPixel(e.pixel, function (f) {
    selected = f;
    selectStyle.getFill().setColor(f.get('COLOR') || '#eeeeee');
    f.setStyle(selectStyle);
    return true;
  });

  if (selected) {
    status.innerHTML = selected.get('ECO_NAME');
  } else {
    status.innerHTML = '&nbsp;';
  }
});
```